using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//public static float Sin(float f);
public class CubeSin : MonoBehaviour
{
    [SerializeField] GameObject Cube;

    private void Awake()
    {
        var n = 15;

        for (int i = 0; i < n; i++)
        {
            var position = new Vector3(i, Mathf.Sin(i), 0f);
            var renderer = Instantiate(Cube, position, Quaternion.identity).GetComponent<MeshRenderer>();
        }
    }
}