using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeFibo : MonoBehaviour
{
    [SerializeField] GameObject Cube;

    private void Awake()
    {
        int first = 0, next = 1, curr = 0;

        var n = 15;

        for (int i = 0; i < n; i++)
        {
            curr = first + next;

            var position = new Vector3(curr, 0f, 0f);
            var renderer = Instantiate(Cube, position, Quaternion.identity).GetComponent<MeshRenderer>();

            first = next;
            next = curr;
        }
    }
}